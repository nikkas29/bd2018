import java.io.File;
import java.io.RandomAccessFile;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.util.Random;

public class GenFile {

    private static int bound = (int) (Math.pow(2,32) - 1);

    private static int length = 0x8000000;
    private static Random random = new Random();

    public static void createFile(String name, long iterationNumber){
        try {
            long ost = iterationNumber;
            FileChannel fileChannel = new RandomAccessFile(new File(name), "rw").getChannel();
            MappedByteBuffer buffer;
            int j = 0;
            while (ost > 0) {
                long chunk = length / Integer.BYTES;
                if (chunk > ost) {
                    chunk = ost;
                }
                buffer = fileChannel.map(FileChannel.MapMode.READ_WRITE,
                        j * chunk * Integer.BYTES,
                        chunk * Integer.BYTES
                );
                for (int i = 0; i < chunk; i++) {
                    int value = random.nextInt(bound);
                    buffer.putInt(value);
                }
                ost -= chunk;
                j++;
            }
            fileChannel.close();
        }
        catch (Exception e){

        }
    }

}
