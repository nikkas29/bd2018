import java.io.IOException;

public class Main {

    public static void main(String[] args) throws IOException, InterruptedException {
        long num;
        num = 550000000;
//        num = 550000; // раскомментировать, для наглядности
        GenFile.createFile("file.bin", num);

        ParallelCalculation.startCalculate("file.bin", num);

        System.out.println("------------------------");

        SequentialCalculation.startCalculate("file.bin", num);
    }

}
