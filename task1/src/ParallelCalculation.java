import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.math.BigInteger;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.util.ArrayList;
import java.util.List;

public class ParallelCalculation extends Thread {
    private static int length = 0x8000000;

    private final static int BOUND = (int)(Math.pow(2,32) - 1);

    private MappedByteBuffer buffer;
    private static long part = length / Integer.BYTES;

    private BigInteger minValue = BigInteger.valueOf(BOUND);
    private BigInteger maxValue = BigInteger.ZERO;
    private BigInteger sum = BigInteger.ZERO;

    private ParallelCalculation(MappedByteBuffer buffer) {
        this.buffer = buffer;
    }

    private BigInteger getSum() {
        return sum;
    }

    private BigInteger getMinValue() {
        return minValue;
    }

    private BigInteger getMaxValue() {
        return maxValue;
    }


    public void run() {
        for (int i = 0; i < part; i++) {
            BigInteger v = BigInteger.valueOf(buffer.getInt());
            if (minValue.compareTo(v) > 0) {
                minValue = BigInteger.valueOf(v.longValue());
            }
            if (maxValue.compareTo(v) < 0) {
                maxValue = BigInteger.valueOf(v.longValue());
            }
            sum = sum.add(v);
        }
    }

    public static void startCalculate(String fileName, long N)
            throws IOException, InterruptedException {

        List<ParallelCalculation> threads = new ArrayList<>();
        List<FileChannel> channels = new ArrayList<>();

        long timeStart, timeEnd;

        long remain = N;
        int i = 0;
        timeStart = System.currentTimeMillis();
        while (remain > 0) {
            FileChannel fileChannel = new RandomAccessFile(new File(fileName), "r").getChannel();
            channels.add(fileChannel);
            if (remain < length / Integer.BYTES) {
                part = remain;
            }
            MappedByteBuffer buffer = fileChannel.map(FileChannel.MapMode.READ_ONLY,
                    i * part * Integer.BYTES,
                    part * Integer.BYTES
            );
            remain -= part;
            threads.add(new ParallelCalculation(buffer));
            threads.get(threads.size() - 1).start();
            i++;
        }

        for (ParallelCalculation thread : threads) {
            thread.join();
        }
        BigInteger totalSum = BigInteger.ZERO;
        BigInteger totalMin = threads.get(0).getMinValue();
        BigInteger totalMax = threads.get(0).getMaxValue();

        for (ParallelCalculation sum : threads) {
            totalSum = totalSum.add(sum.getSum());
            if (totalMin.compareTo(sum.getMinValue()) > 0) {
                totalMin = BigInteger.valueOf(sum.getMinValue().longValue());
            }
            if (totalMax.compareTo(sum.getMaxValue()) < 0) {
                totalMax = BigInteger.valueOf(sum.getMaxValue().longValue());
            }
        }
        timeEnd = System.currentTimeMillis();

        System.out.println("Сумма:" + totalSum.toString());
        System.out.println("Мин.:" + totalMin.toString());
        System.out.println("Макс.:" + totalMax.toString());
        System.out.println("Время последовательного выполнения: " + (timeEnd - timeStart) + " мс");

        for (FileChannel channel : channels) {
            channel.close();
        }
    }

}
