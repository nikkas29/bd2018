import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.math.BigInteger;

public class SequentialCalculation {
    private final static int BOUND = (int)(Math.pow(2,32) - 1);

    static void startCalculate(String fileName, long N) {
        BigInteger sum = BigInteger.ZERO;
        BigInteger minValue = BigInteger.valueOf(BOUND);
        BigInteger maxValue = BigInteger.ZERO;
        long timeStart, timeEnd;
        try {
            DataInputStream dataInputStream = new DataInputStream(new FileInputStream(fileName));
            timeStart = System.currentTimeMillis();
            for(long i = 0; i< N; i++){
                BigInteger v = BigInteger.valueOf(dataInputStream.readInt());
                if (minValue.compareTo(v) > 0) {
                    minValue = BigInteger.valueOf(v.longValue());
                }
                if (maxValue.compareTo(v) < 0) {
                    maxValue = BigInteger.valueOf(v.longValue());
                }
                sum = sum.add(v);
            }
            timeEnd = System.currentTimeMillis();

            System.out.println("Сумма:" + sum.toString());
            System.out.println("Мин.:" + minValue.toString());
            System.out.println("Макс.:" + maxValue.toString());
            System.out.println("Время последовательного выполнения: " + (timeEnd - timeStart) + " мс");

        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }
    }
}
